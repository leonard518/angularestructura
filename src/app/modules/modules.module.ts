import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginModule } from 'src/app/modules/login/login.module';
import { WelcomeModule } from 'src/app/modules/welcome/welcome.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    LoginModule,
    WelcomeModule,
  ]
})
export class ModulesModule { }
